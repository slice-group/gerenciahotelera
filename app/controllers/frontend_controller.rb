class FrontendController < ApplicationController
	layout 'layouts/frontend/application'
	
  def index
  	@message = KepplerContactUs::Message.new
  	@posts = KepplerBlog::Post.where(public: true).order("created_at DESC").first(4)
  end
end
