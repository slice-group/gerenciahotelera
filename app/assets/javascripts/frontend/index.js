$( document ).ready(function() {
	navbarColor()
});
document.onscroll = function() {
	navbarColor()
}
function navbarColor() {
	if($(window).scrollTop() > '70'){
		$('#front-navbar').removeClass('navbar-noColor').addClass('navbar-Color')
	}else{
		$('#front-navbar').removeClass('navbar-Color').addClass('navbar-noColor')
	}
}
$(document).click(function (event) {
	var clickover = $(event.target);
	if ( $(".navbar-collapse").hasClass("in") === true && !clickover.hasClass("navbar-toggle")) {      
		$(".navbar-collapse").collapse('hide');
	}
});

function removeParams() { 
	history.pushState("", document.title, window.location.href.split('#')[0]);
}