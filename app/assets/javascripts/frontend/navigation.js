function scrollToAnchor(anchor){
  $('#'+anchor).animatescroll({
    scrollSpeed:1000,
    padding: 70
  }
)}
$( document ).ready(function() {

  var sections = $('section'), nav = $('.navbar-nav'), nav_height = nav.outerHeight();
 
  $(window).on('scroll', function () {
    animationAbout();
    removeParams();;
    var cur_pos = $(this).scrollTop();
   
    sections.each(function() {
      var top = $(this).offset().top - nav_height,
          bottom = top + $(this).outerHeight();
   
      if (cur_pos >= top && cur_pos <= bottom) {
        nav.find('li').removeClass('current');
        sections.removeClass('current');
   
        $(this).addClass('current');
        nav.find('#bar-'+$(this).attr('id')).addClass('current');
      }
    });
  });

  
});

function animationAbout(){
  if ($(window).width() >= 992 ){
    if( $(window).scrollTop() > $('.about').height() ) {
      $('.about-logo').css('opacity', 1);
      $('.about-logo').addClass('slideInLeft')
      $('.about-text').css('opacity', 1);
      $('.about-text').addClass('slideInRight')
    }
  }
}